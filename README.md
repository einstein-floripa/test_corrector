# Corretor de simulados

## Descrição de código
### Corrector.py:
- lê as respostas dos alunos no simulado
- lê qual o gabarito da prova
- Gera as estatisticas dos alunos
--------
### Templating.py
- Recebe as estatisticas geradas
- Recebe o template em html
- Preenche os templates com as estatisticas
- Salva um pdf para cada candidato

--------

## Inputs do Código

- **gabarito.csv**:

questão | Gabarito | Materia | Dificuldade esperada
-----|-----|-----|-----|
1 | A | Inglês | Díficil
2 | D | Ingles | Fácíl
3 | E | Ingles | Media
4 | ANULADA | Ingles | Facil
5 | B | Ingles | Facil
1 | D | Espanhol | Dificil
2 | C | Espanhol | Facil
3 | E | Espanhol | Media
4 | A | Espanhol | Facil
5 | ANULADA | Espanhol | Facil
6 | A | Literatura | Media
7 | E | Literatura | Media

- **organized_data.csv**:

nome | cpf | idioma | cota | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69 | 70 | 71 | 72 | 73 | 74 | 75 | 76 | 77 | 78 | 79 | 80 | 81 | 82 | 83 | 84 | 85 | 86 | 87 | 88 | 89 | 90 | 91 | 92 | 93 | 94 | 95 | 96 | 97 | 98 | 99 | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 | 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119 | 120 | 121 | 122 | 123 | 124 | 125 | 126 | 127 | 128 | 129 | 130 | 131 | 132 | 133 | 134 | 135 | 136 | 137 | 138 | 139 | 140 | 141 | 142 | 143 | 144 | 145 | 146 | 147 | 148 | 149 | 150 | 151 | 152 | 153 | 154 | 155 | 156 | 157 | 158 | 159 | 160 | 161 | 162 | 163 | 164 | 165 | 166 | 167 | 168 | 169 | 170 | 171 | 172 | 173 | 174 | 175 | 176 | 177 | 178 | 179 | 180
---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
Nome1 Sobrenome1 | 123.456.789-00 | ingles | n_optante | E | A | E | C | A | B | A | D | B | B | A | E | C | D | D | B | A | A | D | B | B | D | D | D | C | B | A | D | C | A | D | B | E | A | A | D | A | D | A | A | C | A | A | E | D | D | B | C | A | B | A | E | E | E | B | C | E | A | E | C | C | D | E | B | C | A | C | A | D | B | C | D | A | C | C | A | A | D | C | C | A | E | E | E | B | D | A | E | E | C | 
Nome2 Sobrenome2 | 123.456.789-01 | espanhol | "cota_less1 | 5_Racial" | D | E | A | C | D | B | D | D | D | A | A | E | C | E | B | C | D | A | E | E | C | B | B | E | B | A | C | D | E | A | A | B | E | C | D | D | A | B | E | A | B | A | E | B | C | D | B | C | D | A | C | D | D | E | E | E | E | A | D | B | C | D | E | B | D | B | B | A | C | C | B | D | A | C | B | B | A | B | A | E | C | E | D | E | B | E | D | E | E | C | A | A | C | D | C | B | B | E | E | E | A | D | A | E | A | C | C | A | B | C | D | A | B | C | A | E | A | D | D | C | D | D | D | D | C | A | A | A | D | B | A | D | A | A | A | C | B | A | D | A | B | D | A | B | D | E | B | E | E | B | E | C | A | A | A | E | B | B | B | E | D | D | C | D | C | B | B | E | D | D | C | A | E | C | A | C | C | A | C | E
Nome3 Sobrenome3 | 123.456.789-02 | espanhol | "cota_less1 | 5_Outros" | D | C | E | A | D | D | E | D | B | A | A | E | C | B | B | C | D | D | D | A | B | D | B | B | E | A | A | B | E | D | E | B | B | A | D | B | A | D | C | A | B | A | D | B | D | E | E | C | C | D | C | E | D | E | B | E | D | A | E | D | C | B | E | A | B | A | B | E | D | C | B | D | B | C | C | B | A | A | A | E | C | E | B | D | D | D | D | A | A | A | D | D | D | D | B | D | A | C | C | B | B | D | E | D | A | E | D | A | C | E | D | A | B | A | C | D | B | E | D | C | C | C | D | A | C | E | A | C | D | B | A | C | C | C | A | D | B | A | C | C | C | D | B | E | D | B | A | B | E | B | E | D | A | B | C | D | A | E | E | E | B | D | B | E | B | C | A | C | D | A | D | B | C | A | A | C | B | E | A | E
Nome4 Sobrenome4 | 123.456.789-03 | ingles | "cota_less1 | 5_Outros" | A | D | E | B | B | C | B | D | D | E | A | E | D | A | B | E | D | B | D | A | A | D | A | D | B | C | A | B | E | A | B | B | C | A | E | D | A | E | E | A | B | A | E | E | C | E | E | A | C | C | C | C | C | B | C | C | C | C | D | E | C | B | A | C | C | D | B | B | D | C | C | C | C | C | C | C | B | C | C | C | C | C | C | C | A | A | E | C | D | C | A | B | E | A | C | A | C | B | D | A | E | C | E | C | D | D | E | D | C | B | C | D | B | D | A | E | C | D | B | D | C | C | D | B | D | E | C | E | E | E | D | E | C | A | C | C | A | C | C | B | B | D | C | C | B | E | C | E | C | D | C | C | D | D | C | C | E | E | C | C | C | D | D | A | C | D | A | A | C | C | B | C | A | A | A | C | E
Nome5 Sobrenome5 | 123.456.789-05 | espanhol | "cota_less1 | 5_Outros" | D | C | E | A | C | D | D | E | A | A | E | C | C | D | C | C | B | D | A | D | D | A | C | A | C | A | D | E | A | E | E | A | A | A | D | A | D | B | A | B | A | A | B | C | A | B | C | B | A | D | E | C | B | D | B | B | A | A | E | E | B | E | E | C | A | B | E | D | C | B | D | D | C | D | B | A | A | E | C | A | E | C | C | B | D | D | A | A | C | D | D | A | D | B | C | E | C | B | E | B | D | E | B | A | E | E | A | C | C | D | B | A | E | B | C | D | A | D | B | B | D | D | C | A | E | A | C | C | C | E | C | D | A | C | B | D | C | A | E | D | B | A | D | C | B | B | E | A | A | C | A | E | B | D | A | B | E | C | A | C | E | A | B | D | A | B | D | E | B | C | E | A | D | C | D | B | B
