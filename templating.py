# ## Templating with Jinja2
# 
# Here is the basics components we're going to use:
# - **string_tamplate :** It's a string containing the text/html with tags showing where it's expected a variable
# - **template = Enviroment() :** It's the main object of Jinja2
# - **context = {} :** It's a Dict where the keys are the variable name (the same ones contained in string_tamplate) and the values are the replacement variable
# 
# ```
# string_tamplate = """ 
# Hello, {{ name }}! How are you?
# That is a simples template and I will list some things:
# {% for word in words %}
# > {{ word }}
# {% endfor %}
# """
# template = Environment().from_string(source=string_template) 
# context = {
#     'name' : 'Genious',
#     'words' : [
#         'Pizza',
#         'Data',
#         'Storytelling',
#         'Exploratory Data Analysis',
#     ],
# }
# 
# rendered = template.render(context)
# ```

from jinja2 import Environment
from weasyprint import HTML, CSS
from corrector import results, subjects, score_english, score_spanish, data_english, data_spanish
import pandas as pd 
import os
 
# Input de qual é o simulado que está sendo corrigido
# test_name = input('Digite o nome desse simulado. Ex.: simulufsc, simuludesc, simulenem: ')
# test_name = test_name.upper().strip()

# Abre e lê o template criado em html
# Nesse arquivo .html contém as marcações do template.
#  Exemplo de marcação: {{ name }}
string_template = open('template/template.html').read()

# subjects é uma Series importada do corrector que contém todas 
# as matérias contidas nesse simulado
materias = subjects.to_list()

# Transforma o template criado em html em um objeto do jinja2
template = Environment().from_string(source=string_template)

#css = CSS(filename=feedback_template.css, font_config=font_config)

# Loop para geração de todos os PDF's
# A iteração é realizada sobre todos os CPFs, cada cpf corresponde a um candidato
for cpf in results.index:

    # student é uma Series com todas as informações do candidato
    student = results.loc[cpf]

    # grades é um dicionário com o nome das matérias como key e as notas como values
    grades = student[materias].dropna().to_dict() 
    
    # Dicionário de referência para completar o template
    context = {
        'name':student['nome'],
        'cpf': cpf,
        'idiom': student['idioma'],
        'quota' : student['cota'],
        'materias' : materias,
        'notas' : grades,
        'pontuacao_total' : student['Total']
    }

    # Renderiza o template
    rendered = template.render(context)

    # Transforma o renderizado em um pdf
    HTML(string=rendered,
         base_url=os.getcwd()).write_pdf(target=os.getcwd()+'/pdf/'+cpf+'.pdf')
                                       # stylesheets=css,
                                        #font_config=font_config


