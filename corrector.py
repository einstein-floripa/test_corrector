#!/usr/bin/env python
# coding: utf-8

from pandas import DataFrame, Series
import pandas as pd
import regex as re  

# ======================================================================
# ### input and reading
# target_path = input('Escreva o caminho do arquivo que contem o gabarito: ')
# organized_data_path = input('Escreva o caminho do arquivo que contem os dados vindos da matrix: ')

# Considerando que o numero da questao seja a primeira coluna
target = pd.read_csv('inputs/gabarito.csv',index_col=0) 

#Considerando que o nome da coluna do cpf venha no formato: 'cpf'
organized_data = pd.read_csv('inputs/organized_data.csv', index_col='cpf')

# ======================================================================
# ### Cleaning the entries
# - [ ] Aplicar strip e captalize em todos os indices
# - [ ] Retirar todos os acentos

substitute = {
    'á':'a',
    'à':'a',
    'ã':'a',
    'â':'a',
    'é':'e',
    'è':'e',
    'ẽ':'e',
    'ê':'e',
    'í':'i',
    'ì':'i',
    'ĩ':'i',
    'î':'i',
    'ó':'o',
    'ò':'o',
    'õ':'o',
    'ô':'o',
    'ú':'u',
    'ù':'u',
    'ũ':'u',
    'û':'u',
    'Á':'A',
    'À':'A',
    'Ã':'A',
    'Â':'A',
    'É':'E',
    'È':'E',
    'Ẽ':'E',
    'Ê':'E',
    'Í':'I',
    'Ì':'I',
    'Ĩ':'I',
    'Î':'I',
    'Ó':'O',
    'Ò':'O',
    'Õ':'O',
    'Ô':'O',
    'Ú':'U',
    'Ù':'U',
    'Ũ':'U',
    'Û':'U'    
}

re.ASCII
def remover_acentos(string, substitute=substitute):
    acento_treat = string.strip()
    
    for old,new  in substitute.items(): 
        acento_treat = re.sub(old,new, acento_treat)
        
    return acento_treat.capitalize()

target = target.applymap(remover_acentos)

# ======================================================================
# ### Manipulando o gabarito
# - [x] Dividir os gabaritos de ingles e espanhol

# Separando o dataframe gabarito da prova de ingles e de espanhol
english = target[target['Materia']!='Espanhol']
spanish = target[target['Materia']!='Ingles']

# Listando as materias presentes no simulado
subjects = target['Materia'].drop_duplicates()

# Contando o numero de questões em cada materia
subject_questions_amount = target['Materia'].value_counts()

# Isolando os gabaritos de cada prova em uma Series
target_english = english['Gabarito']
target_spanish = spanish['Gabarito']

# ======================================================================
# ### Manipulando os dados
# - [x] Reindexar para cpf's
# - [x] Dividir os alunos por opcao de idioma
# - [x] Isolar as respostas em outro DataFrame
# - [x] Criar um dataframe para guardar os scores


# Separando os alunos por idioma escolido em DataFrames diferentes
english_students = organized_data[organized_data['idioma']=='ingles']
spanish_students = organized_data[organized_data['idioma']=='espanhol']

# Isolando somente as respostas dos DataFrames dos alunos
data_english = english_students.loc[:,'1':]
data_spanish = spanish_students.loc[:,'1':]
data_english.to_csv('results/data_english.csv')

def corrector(resposta, target):
    """Funcao de correcao que sera usada no apply. O valor de return sera a pontuacao na questao."""
    if resposta==target:
        return 1
    elif target=='Anulada':
        return 1
    elif pd.isna(resposta):
        return None
    else:
        return 0

# Criando cópias para manipulacao nas correcoes
score_english = data_english.copy()
score_spanish = data_spanish.copy()

for (numero, target) in target_english.to_dict().items():
    """Loop de correcao e formacao do DataFrame Score de ingles"""
    pontuacao = data_english[str(numero)].apply(corrector, target=target) # pontuacao da questao
    score_english[str(numero)] = pontuacao # salva a pontuacao no dataframe score_english
    
for (numero, target) in target_spanish.to_dict().items():
    """Loop de correcao e formacao do DataFrame Score de espanhol"""
    pontuacao = data_spanish[str(numero)].apply(corrector, target=target)
    score_spanish[str(numero)] = pontuacao

# ======================================================================
# ### Gerando as estatisticas relacionadas a cada cpf
# - [x] Pontuação total por cpf
# - [x] Pontuação absoluta por materia
# - [x] Pontuação em porcentagem por cpf
# - [x] Pontuação em porcentagem por materia de cada cpf

# Pontuacao total por cpf
total_score_english = score_english.sum(axis=1)
total_score_spanish = score_spanish.sum(axis=1)

# Pontuacao por materia por cpf dos candidados que fizeram a prova de ingles
cp_score_english = score_english
subject_score_english = DataFrame(index=score_english.index)
cp_score_english.columns = english['Materia'].values

for subject in cp_score_english.columns.unique():
    subject_score_english[subject] = cp_score_english[subject].sum(axis=1)
    
# Pontuacao por materia por cpf dos candidados que fizeram a prova de espanhol
cp_score_spanish = score_spanish
subject_score_spanish = DataFrame(index=score_spanish.index)
cp_score_spanish.columns = spanish['Materia'].values

for subject in cp_score_spanish.columns.unique():
    subject_score_spanish[subject] = cp_score_spanish[subject].sum(axis=1)

# Pontuação em porcentagem por cpf
def Calculate_percentage(grade, questions_amount):
    """Funçao para calcular a média de cada matéria"""
    grade_in_percentage = grade/questions_amount
    
    return grade_in_percentage

for subject in subject_score_english.columns:
    """Loop para criar colunas no DataFrame de scores de ingles com as pontuações em porcentagem"""
    # Aplica a funçao para calcular a porcentagem no DataFrame de score de ingles
    grade_in_percentage = subject_score_english[subject].apply(Calculate_percentage, # Aplicando a funçao
                                                                questions_amount=subject_questions_amount[subject]) # Parametro de quantas questões tem a matéria
    # Salva o valor em porcentagem no dataframe de Score
    subject_score_english['percent_'+subject] = grade_in_percentage 

for subject in subject_score_spanish.columns:
    """Loop para criar colunas no DataFrame de scores de espanhol com as pontuações em porcentagem"""
    # Aplica a funçao para calcular a porcentagem no DataFrame de score de espanhol
    grade_in_percentage = subject_score_spanish[subject].apply(Calculate_percentage,
                                                            questions_amount=subject_questions_amount[subject])
    # Salva o valor em porcentagem no dataframe de Score
    subject_score_spanish['percent_'+subject] = grade_in_percentage

# Adicionando uma coluna com a pontuacao total
subject_score_english['Total'] = total_score_english
subject_score_spanish['Total'] = total_score_spanish

# ### Juntar os DataFrames e readicionar as informações dos alunos
# - [x] Adicionar as informações dos alunos aos dataframes com as notas
# - [x] Juntar os DataFrames
# - [x] Ordenar os alunos de acordo com a pontuação total

# Isolando as informacoes dos alunos
english_students_info = english_students.loc[:,:'1']
spanish_students_info = spanish_students.loc[:,:'1']

# Juntando as colunas de informacoeas com as pontuacoes em cada materia e total
results_english = english_students_info.join(subject_score_english)
results_spanish = spanish_students_info.join(subject_score_spanish)

# Criando uma coluna com o nome cpf para poder fazer o merge
results_english['cpf'] = results_english.index
results_spanish['cpf'] = results_spanish.index

# Reindexando os dataframes para fazer o merge
results_english = results_english.set_index(pd.Int64Index(range(results_english.index.size)))
results_spanish = results_spanish.set_index(pd.Int64Index(range(results_spanish.index.size)))

# Executando o merge dos DataFrames com resultados dos alunos que fizeram a prova de ingles e espanhol
# Reindexando DataFrame criado para deixar os cpfs como index
results = results_spanish.merge(results_english, how='outer')
results = results.set_index(results['cpf'])

# Retirando a coluna cpf e da questão '1' que nao sao necessarias
results = results.drop(['cpf', '1'], axis=1)

# Gerando a porcentagem do total
results['percent_Total'] = results['Total'].apply(Calculate_percentage,
                                    questions_amount=subject_questions_amount.drop('Espanhol').sum())


# ======================================================================
# ### Gerando as estatisticas relacionadas a cada matéria
# - [ ] Média de acerto por questão
# - [x] Média geral por matéria
# - [x] Pontuação em porcentagem por materia geral

# Média geral por matéria
subjects_report = DataFrame(results.describe().T)
subjects_report.to_csv('results/subjects_report.csv')

# Média de acerto por questão
score_english.mean().to_csv('test_csv/score_english.csv', header=True)


# ======================================================================
# Classe criada para facilitar o import no arquivo de templates
# ex.: 
# from corrector import Results
# enem2019 = Results()

class Results():
    """
    Classe gerada para armazenar os resultados gerados pelo script 
    de correção e facilitar na importação para o template
    """
    def __init__(self,  results=results, 
                        subjects=subjects,
                        score_by_question_english=score_english,
                        score_by_question_spanish=score_spanish,
                        answer_by_question_english=data_english,
                        answer_by_question_spanish=data_spanish,
                        subjects_report=subjects_report):
        
        self._results = results
        self._subjects = subjects
        self._score_by_question_english = score_by_question_english
        self._score_by_question_spanish = score_by_question_spanish
        self._answer_by_question_english = answer_by_question_english
        self._answer_by_question_spanish = answer_by_question_spanish
        self._subjects_report = subjects_report
    
    def outputs_to_csv(self):
        self._results.to_csv('results/results.csv')
        self._subjects.to_csv('results/subjects.csv')  
        self._score_by_question_english.to_csv('results/score_by_question_english.csv')  
        self._score_by_question_spanish.to_csv('results/score_by_question_spanish.csv')
        self._answer_by_question_english.to_csv('results/answer_by_question_english.csv')
        self._answer_by_question_spanish.to_csv('results/answer_by_question_spanish.csv')
        self._subjects_report.to_csv('results/subjects_report.csv')

    def results(self):
        """
        Type: pd.DataFrame
        Funcionalidade: armazenar os resultados por aluno

        Organização:
            index = CPFs dos alunos que fizeram a prova
            columns = [cpf,nome,idioma,cota,Espanhol,Literatura,Portugues,Historia,Geografia,Filosofia/sociologia,Biologia,Fisica,Quimica,Matematica,percent_Espanhol,percent_Literatura,percent_Portugues,percent_Historia,percent_Geografia,percent_Filosofia/sociologia,percent_Biologia,percent_Fisica,percent_Quimica,percent_Matematica,Total,Ingles,percent_Ingles,percent_Total]
        
        Obs.: As colunas cujo nome começa com 'percent_' contem as as notas em porcentagem
        """
        
        return self._results
    
    def subjects(self):
        """
        Type: pd.Series
        Funcionalidade: armazenar as matérias contidas no simulado

        Organização:
            index = Primeira questão da matéria
            values =  Materias
        """

        return self._subjects
    
    def score_by_question_english(self):
        """
        Type: pd.DataFrame
        Funcionalidade: armazenar as matérias contidas no simulado

        Organização:
            index = CPFs dos alunos que fizeram a prova
            columns =  numero das questões
        """
        
        return self._score_by_question_english
    
    def score_by_question_spanish(self):
        """
        Type: pd.DataFrame
        Funcionalidade: Armazenar as pontuações obtidas por cada candidato por questão | alunos que fizeram a prova de espanhol

        Organização:
            index = CPFs dos alunos que fizeram a prova
            columns =  numero das questões
        """
        
        return self._score_by_question_spanish
    
    def answer_by_question_english(self):
        """
        Type: pd.DataFrame
        Funcionalidade: armazenar as respostas dadas por cada candidato que fez a prova de ingles

        Organização:
            index = CPFs dos alunos que fizeram a prova
            columns =  numero das questões
        """
        
        return self._answer_by_question_english

    def answer_by_question_spanish(self):
        """
        Type: pd.DataFrame
        Funcionalidade: armazenar as respostas dadas por cada candidato que fez a prova de espanhol

        Organização:
            index = CPFs dos alunos que fizeram a prova
            columns =  numero das questões  
        """
        
        return self._answer_by_question_spanish
    
    def subjects_report(self):
        """
        Type: pd.DataFrame
        Funcionalidade: armazenar as estatísticas por matéria

        Organização:
            index = nome das matérias para estatisticas com valores absolunos e percent_matéria para estatisticas em percentual
            columns =  [count, mean, std, min, 25%, 50%, 75%, max]
        """

        return self._subjects_report